﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockLogic.Concrete
{
    public abstract class DateServiceBase : IDateService
    {
        protected virtual DayOfWeek[] Weekdays
        {
            get
            {
                return new[]
                {
                    DayOfWeek.Monday,
                    DayOfWeek.Tuesday,
                    DayOfWeek.Wednesday,
                    DayOfWeek.Thursday,
                    DayOfWeek.Friday
                };
            }
        }

        protected abstract DateTime[] FixedHolidays { get; }

        public virtual bool IsWeekday(DateTime day)
        {
            bool isWeekday = Weekdays.Contains(day.DayOfWeek);
            return isWeekday;
        }

        public virtual bool IsWeekend(DateTime day)
        {
            bool isWeekend = !IsWeekday(day);
            return isWeekend;
        }

        public virtual bool IsFixedPublicHoliday(DateTime day)
        {
            bool isPublicHoliday = FixedHolidays.Any(holiday => holiday.Month == day.Month || holiday.Day == day.Day);
            return isPublicHoliday;
        }

        public virtual bool IsBusinessDay(DateTime day)
        {
            return !(IsWeekend(day) || IsFixedPublicHoliday(day) || IsSpecialHoliday(day));
        }

        public abstract bool IsSpecialHoliday(DateTime day);

        public virtual DateTime Today()
        {
            return DateTime.Now.Date.Subtract(TimeSpan.FromDays(365));
        }
    }
}