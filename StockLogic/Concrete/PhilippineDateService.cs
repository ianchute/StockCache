﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockLogic.Concrete
{
    public class PhilippineDateService : DateServiceBase
    {
        protected override DateTime[] FixedHolidays
        {
            get
            {
                return new[]
                {
                    // New Year's Day.
                    new DateTime(1, 1, 1),

                    // Day of Valor.
                    new DateTime(1, 4, 9),

                    // Labor Day.
                    new DateTime(1, 5, 1),

                    // Bonifacio Day.
                    new DateTime(1, 11, 30),

                    // Christmas Day.
                    new DateTime(1, 12, 25),

                    // Rizal Day.
                    new DateTime(1, 12, 30)
                };
            }
        }

        public override bool IsSpecialHoliday(DateTime day)
        {
            // TODO
            return false;
        }
    }
}