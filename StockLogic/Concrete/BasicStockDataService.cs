﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StockLogic.Models;

namespace StockLogic.Concrete
{
    public class BasicStockDataService : IBasicStockDataService
    {
        private StockData.IStockDataProvider provider;
        private IMapper<StockData.Models.Stock, Stock> mapper;
        private IDateService dateService;
        private ISymbolProvider symbolProvider;

        public BasicStockDataService(
            StockData.IStockDataProvider provider, 
            IMapper<StockData.Models.Stock, Stock> mapper,
            IDateService dateService,
            ISymbolProvider symbolProvider)
        {
            this.provider = provider;
            this.mapper = mapper;
            this.dateService = dateService;
            this.symbolProvider = symbolProvider;
        }

        public Models.Stock GetCurrentStockData(string symbol)
        {
            DateTime today = dateService.Today();
            Stock stock = GetStockData(symbol, today);
            return stock;
        }

        public Models.Stock GetStockData(string symbol, DateTime date)
        {
            StockData.Models.Stock dataModel = provider.GetStockData(symbol, date);
            Stock stock = mapper.Map(dataModel);
            return stock;
        }

        public IEnumerable<Stock> GetHistoricalStockData(string symbol)
        {
            int maxDaysBacktrack = Settings.MaxDaysBacktrack;
            DateTime currentDate = dateService.Today();
            List<DateTime> datesToGet = new List<DateTime>(maxDaysBacktrack);

            while(maxDaysBacktrack != 0)
            {
                if(dateService.IsBusinessDay(currentDate))
                {
                    datesToGet.Add(currentDate);
                    --maxDaysBacktrack;
                }
                currentDate = currentDate.Subtract(TimeSpan.FromDays(1));
            }

            IEnumerable<Stock> historicalStockData = datesToGet
                .OrderByDescending(_ => _)
                .Select(_ => GetStockData(symbol, _))
                .Where(_ => _.AsOf != DateTime.MinValue) // TODO: Create test.
                .ToArray();

            return historicalStockData;
        }

        public IEnumerable<Stock> GetAllCurrentStockData()
        {
            IEnumerable<string> stockSymbols = symbolProvider.GetStockSymbols();
            Stock[] allStockData = stockSymbols
                .Select(GetCurrentStockData)
                .Where(_ => _.AsOf != DateTime.MinValue)
                .ToArray();

            return allStockData;
        }
    }
}
