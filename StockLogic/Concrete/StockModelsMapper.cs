﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StockLogic.Models;

namespace StockLogic.Concrete
{
    public class StockModelsMapper : IMapper<StockData.Models.Stock, Stock>
    {
        public Stock Map(StockData.Models.Stock _)
        {
            if (!_.IsValid)
                return new Stock();

            Stock stock = new Stock()
            {
                AsOf = _.AsOf,
                Currency = _.Currency,
                Name = _.Name,
                SharePrice = _.SharePrice,
                Symbol = _.Symbol,
                Volume = _.Volume
            };

            return stock;
        }
    }
}
