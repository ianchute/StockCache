﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockLogic
{
    public interface IDateService
    {
        bool IsWeekday(DateTime day);

        bool IsWeekend(DateTime day);

        bool IsFixedPublicHoliday(DateTime day);

        bool IsSpecialHoliday(DateTime day);

        bool IsBusinessDay(DateTime day);

        DateTime Today();
    }
}