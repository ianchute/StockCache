﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockLogic.Models
{
    public class Stock
    {
        public string Symbol { get; set; }

        public string Name { get; set; }

        public double SharePrice { get; set; }

        public int Volume { get; set; }

        public string Currency { get; set; }

        public DateTime AsOf { get; set; }
    }
}