﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StockLogic.Models;

namespace StockLogic
{
    public interface IBasicStockDataService
    {
        Stock GetCurrentStockData(string symbol);

        Stock GetStockData(string symbol, DateTime date);

        IEnumerable<Stock> GetHistoricalStockData(string symbol);

        IEnumerable<Stock> GetAllCurrentStockData();
    }
}