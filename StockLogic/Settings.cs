﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace StockLogic
{
    public static class Settings
    {
        static NameValueCollection _ = ConfigurationManager.AppSettings;

        public static int MaxDaysBacktrack
        {
            get
            {
                int result = 180;
                string timeout = _["maxDaysBacktrack"];
                int.TryParse(timeout, out result);
                return result;
            }
        }
    }
}
