﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockCache.Models
{
    public class StockVM
    {
        public string Symbol { get; set; }

        public string Name { get; set; }

        public double SharePrice { get; set; }

        public int Volume { get; set; }

        public string Currency { get; set; }

        public DateTime AsOf { get; set; }
    }
}
