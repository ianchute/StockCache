﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StockCache.Models;
using SL = StockLogic;

namespace StockCache.Concrete
{
    public class StockViewModelMapper : IMapper<SL.Models.Stock, StockVM>
    {
        public StockVM Map(SL.Models.Stock _)
        {
            StockVM vm = new StockVM()
            {
                AsOf = _.AsOf,
                Currency = _.Currency,
                Name = _.Name,
                SharePrice = _.SharePrice,
                Symbol = _.Symbol,
                Volume = _.Volume
            };

            return vm;
        }
    }
}
