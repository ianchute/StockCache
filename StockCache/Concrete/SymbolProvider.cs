﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SL = StockLogic;

namespace StockCache.Concrete
{
    public class SymbolProvider : SL.ISymbolProvider
    {
        public IEnumerable<string> GetStockSymbols()
        {
            return ConfigurationManager.AppSettings["stockSymbols"]
                .Split(' ')
                .OrderBy(_ => _);
        }
    }
}