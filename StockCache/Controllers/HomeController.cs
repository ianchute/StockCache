﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StockCache.Models;
using SL = StockLogic;

namespace StockCache.Controllers
{
    public class HomeController : Controller
    {
        private SL.IBasicStockDataService service;
        private IMapper<SL.Models.Stock, Models.StockVM> mapper;

        public HomeController(
            SL.IBasicStockDataService service, 
            IMapper<SL.Models.Stock, StockVM> mapper)
        {
            this.service = service;
            this.mapper = mapper;
        }

        public ActionResult Index()
        {
            IEnumerable<SL.Models.Stock> allStockData = service.GetAllCurrentStockData();
            StockVM[] viewModels = allStockData
                .Select(mapper.Map)
                .ToArray();
            return View(viewModels);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Stock Cache";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Stock Cache";

            return View();
        }
    }
}