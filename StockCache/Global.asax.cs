﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using SimpleInjector;
using SimpleInjector.Integration.Web.Mvc;
using StockCache.Concrete;
using StockCache.Models;
using SD = StockData;
using SL = StockLogic;

namespace StockCache
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            var container = new Container();
            WireupDataLayer(container);
            WireupLogicLayer(container);
            WireupAppLayer(container);
            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            container.RegisterMvcIntegratedFilterProvider();
            container.Verify();

            OptimizeWebRequests();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }

        private void OptimizeWebRequests()
        {
            ServicePointManager.Expect100Continue = false;
            ServicePointManager.DefaultConnectionLimit = 9999;
            WebRequest.DefaultWebProxy = GlobalProxySelection.GetEmptyWebProxy();
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
        }

        private void WireupDataLayer(Container _)
        {
            _.Register<SD.IFormatter<DateTime>, SD.Concrete.ShortDateFormatter>();
            _.Register<SD.IFormatter<string>, SD.Concrete.SymbolFormatter>();
            _.Register<SD.IMapper<string, SD.Models.Stock>, SD.Concrete.PSEi_XML_StringResponseToStockMapper>();
            _.Register<SD.IParameterBuilder, SD.Concrete.PSEi_ParameterBuilder>();
            _.Register<SD.IStockDataProvider, SD.Concrete.PSEi_StockDataProvider>();
            _.Register<SD.IUriBuilder, SD.Concrete.PSEi_UriBuilder>();
            _.Register<SD.IWebClient, SD.Concrete.WebRequestWrapper>();
        }

        private void WireupLogicLayer(Container _)
        {
            _.Register<SL.IBasicStockDataService, SL.Concrete.BasicStockDataService>();
            _.Register<SL.IDateService, SL.Concrete.PhilippineDateService>();
            _.Register<SL.IMapper<SD.Models.Stock, SL.Models.Stock>, SL.Concrete.StockModelsMapper>();
            _.Register<SL.ISymbolProvider, Concrete.SymbolProvider>();
        }

        private void WireupAppLayer(Container _)
        {
            _.Register<IMapper<SL.Models.Stock, StockVM>, StockViewModelMapper>();
        }
    }
}