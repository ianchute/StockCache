﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockCache
{
    public interface IMapper<T, U>
        where T : class
        where U : class
    {
        U Map(T _);
    }
}
