﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StockLogic.Concrete;

namespace StockLogic.Tests
{
    [TestClass]
    public class PhilippineDateServiceTests
    {
        PhilippineDateService target;

        [TestInitialize]
        public void Initialize()
        {
            target = new PhilippineDateService();
        }

        [TestCleanup]
        public void Cleanup()
        {
            target = null;
        }

        [TestMethod]
        public void PhilippineDateServiceShouldConstruct()
        {
        }

        [TestMethod]
        public void PhilippineDateServiceShouldCorrectlyIdentifyWeekdays()
        {
            DateTime sample = new DateTime(2015, 7, 24); // Friday

            bool result = target.IsWeekday(sample);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void PhilippineDateServiceShouldCorrectlyIdentifyWeekends()
        {
            DateTime sample = new DateTime(2015, 7, 25); // Saturday

            bool result = target.IsWeekend(sample);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void PhilippineDateServiceShouldCorrectlyIdentifyPublicFixedHolidays()
        {
            DateTime sample = new DateTime(2015, 12, 25); // Christmas (Friday)

            bool result = target.IsFixedPublicHoliday(sample);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void PhilippineDateServiceShouldCorrectlyIdentifySpecialHolidays()
        {
            DateTime sample = new DateTime(2015, 2, 19); // Chinese New Year (Thursday)

            bool result = target.IsFixedPublicHoliday(sample);

            // TODO
            //Assert.IsTrue(result);
        }

        [TestMethod]
        public void PhilippineDateServiceShouldCorrectlyIdentifyBusinessDaysThatAreWeekdays()
        {
            DateTime sample = new DateTime(2015, 7, 24); // Friday

            bool result = target.IsBusinessDay(sample);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void PhilippineDateServiceShouldCorrectlyIdentifyNonBusinessDaysThatAreWeekdays()
        {
            DateTime sample = new DateTime(2015, 11, 30); // Monday but Bonifacio Day

            bool result = target.IsBusinessDay(sample);

            Assert.IsFalse(result);
        }
    }
}
