﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using StockLogic.Concrete;
using StockLogic.Models;
using System.Configuration;

namespace StockLogic.Tests
{
    [TestClass]
    public class BasicStockDataServiceTests
    {
        StockData.IStockDataProvider provider;
        IMapper<StockData.Models.Stock, Stock> mapper;
        IDateService dateService;
        ISymbolProvider symbolProvider;

        BasicStockDataService target;

        static readonly DateTime today = new DateTime(2015, 7, 24); // Friday Non-holiday
        StockData.Models.Stock sampleStock_today = new StockData.Models.Stock()
        {
            AsOf = today,
            Currency = "PHP",
            Name = "Ayala Land",
            SharePrice = 12.34,
            Symbol = "ALI",
            Volume = 1234,
            IsValid = true
        };
        StockData.Models.Stock sampleStock = new StockData.Models.Stock()
        {
            Currency = "PHP",
            Name = "DM Consunji",
            SharePrice = 23.45,
            Symbol = "DMC",
            Volume = 2345,
            IsValid = true
        };
        DateTime[] historicalDates = new []
        {
            new DateTime(2015, 7, 13), 
            new DateTime(2015, 7, 14), 
            new DateTime(2015, 7, 15), 
            new DateTime(2015, 7, 16), 
            new DateTime(2015, 7, 17),
            new DateTime(2015, 7, 20), 
            new DateTime(2015, 7, 21), 
            new DateTime(2015, 7, 22), 
            new DateTime(2015, 7, 23), 
            new DateTime(2015, 7, 24),
        };

        [TestInitialize]
        public void Initialize()
        {
            ConfigurationManager.AppSettings["maxDaysBacktrack"] = historicalDates.Length.ToString();
            
            provider = Substitute.For<StockData.IStockDataProvider>();
            provider.GetStockData(sampleStock_today.Symbol, today)
                .Returns<StockData.Models.Stock>(sampleStock_today);
            provider.GetStockData(sampleStock.Symbol, Arg.Any<DateTime>())
                .Returns<StockData.Models.Stock>(_ =>
                {
                    sampleStock.AsOf = _.Arg<DateTime>().Date;
                    return sampleStock;
                });

            mapper = Substitute.For<IMapper<StockData.Models.Stock, Stock>>();
            mapper
                .Map(Arg.Any<StockData.Models.Stock>())
                .Returns<Stock>(_ =>
                {
                    var stock = _.Arg<StockData.Models.Stock>();
                    return new Stock()
                    {
                        AsOf = stock.AsOf,
                        Currency = stock.Currency,
                        Name = stock.Name,
                        SharePrice = stock.SharePrice,
                        Symbol = stock.Symbol,
                        Volume = stock.Volume
                    };
                });

            dateService = Substitute.For<IDateService>();
            dateService
                .IsBusinessDay(Arg.Any<DateTime>())
                .Returns<bool>(_ =>
                {
                    var dayOfWeek = _.Arg<DateTime>().DayOfWeek;
                    return dayOfWeek != DayOfWeek.Saturday
                        && dayOfWeek != DayOfWeek.Sunday;
                });
            dateService.Today().Returns(today);

            // TODO
            symbolProvider = Substitute.For<ISymbolProvider>();

            target = new BasicStockDataService(provider, mapper, dateService, symbolProvider);
        }

        [TestCleanup]
        public void Cleanup()
        {
            target = null;
            provider = null;
            mapper = null;
            dateService = null;
        }

        [TestMethod]
        public void BasicStockDataServiceShouldConstruct()
        {
        }

        [TestMethod]
        public void BasicStockDataServiceGetCurrentStockDataShouldReturnCorrectData()
        {
            string sampleSymbol = sampleStock_today.Symbol;

            Stock result = target.GetCurrentStockData(sampleSymbol);

            Assert.AreEqual(sampleStock_today.Currency, result.Currency);
            Assert.AreEqual(sampleStock_today.Name, result.Name);
            Assert.AreEqual(sampleStock_today.SharePrice, result.SharePrice);
            Assert.AreEqual(sampleStock_today.Symbol, result.Symbol);
            Assert.AreEqual(sampleStock_today.Volume, result.Volume);
            Assert.AreEqual(sampleStock_today.AsOf.ToShortDateString(), result.AsOf.ToShortDateString());
        }

        [TestMethod]
        public void BasicStockDataServiceGetStockDataShouldReturnCorrectData()
        {
            string sampleSymbol = sampleStock.Symbol;
            DateTime sampleDate = new DateTime(2015, 7, 20); // Monday Non-Holiday

            Stock result = target.GetStockData(sampleSymbol, sampleDate);

            Assert.AreEqual(sampleStock.Currency, result.Currency);
            Assert.AreEqual(sampleStock.Name, result.Name);
            Assert.AreEqual(sampleStock.SharePrice, result.SharePrice);
            Assert.AreEqual(sampleStock.Symbol, result.Symbol);
            Assert.AreEqual(sampleStock.Volume, result.Volume);
            Assert.AreEqual(sampleStock.AsOf.ToShortDateString(), sampleDate.ToShortDateString());
        }

        [TestMethod]
        public void BasicStockDataServiceGetHistoricalStockDataShouldReturnCorrectData()
        {
            string sampleSymbol = sampleStock.Symbol;

            IEnumerable<Stock> result = target.GetHistoricalStockData(sampleSymbol);

            bool sameNumber = result.Count() == historicalDates.Length;
            bool allDatesContained = result.Select(_ => _.AsOf).All(historicalDates.Contains);
            bool allDatesContained_vv = historicalDates.All(result.Select(_ => _.AsOf).Contains);
            bool dataCorrect = result
                .All(_ =>
                    sampleStock.Currency == _.Currency
                    && sampleStock.Name == _.Name
                    && sampleStock.SharePrice == _.SharePrice
                    && sampleStock.Symbol == _.Symbol
                    && sampleStock.Volume == _.Volume
                );

            Assert.IsTrue(sameNumber && allDatesContained && allDatesContained_vv && dataCorrect);
        }

        [TestMethod]
        public void BasicStockDataServiceGetAllCurrentStockDataShouldReturnCorrectData()
        {
            // TODO
        }
    }
}