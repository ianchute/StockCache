﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StockLogic.Concrete;
using StockLogic.Models;

namespace StockLogic.Tests
{
    [TestClass]
    public class StockModelsMapperTests
    {
        StockModelsMapper target;

        [TestInitialize]
        public void Initialize()
        {
            target = new StockModelsMapper();
        }

        [TestCleanup]
        public void Cleanup()
        {
            target = null;
        }

        [TestMethod]
        public void StockModelsMapperShouldConstruct()
        {
        }

        [TestMethod]
        public void StockModelsMapperShouldMapCorrectly()
        {
            StockData.Models.Stock sampleStock = new StockData.Models.Stock()
            {
                AsOf = new DateTime(2014, 07, 22),
                Currency = "PHP",
                Name = "Ayala Land",
                SharePrice = 30.75,
                Symbol = "ALI",
                Volume = 4350500,
                IsValid = true
            };

            Stock result = target.Map(sampleStock);

            Assert.AreEqual(sampleStock.Currency, result.Currency);
            Assert.AreEqual(sampleStock.Name, result.Name);
            Assert.AreEqual(sampleStock.SharePrice, result.SharePrice);
            Assert.AreEqual(sampleStock.Symbol, result.Symbol);
            Assert.AreEqual(sampleStock.Volume, result.Volume);
            Assert.AreEqual(sampleStock.AsOf.ToShortDateString(), result.AsOf.ToShortDateString());
        }

        public void StockModelsMapperShouldMapToEmptyStockWhenSourceStockIsInvalid()
        {
            StockData.Models.Stock sampleStock = new StockData.Models.Stock()
            {
                IsValid = false
            };

            Stock result = target.Map(sampleStock);

            Assert.AreEqual(null, result.Currency);
            Assert.AreEqual(null, result.Name);
            Assert.AreEqual(0.0, result.SharePrice);
            Assert.AreEqual(null, result.Symbol);
            Assert.AreEqual(0, result.Volume);
            Assert.AreEqual(default(DateTime).ToShortDateString(), result.AsOf.ToShortDateString());
        }
    }
}
