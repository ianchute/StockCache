﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockData
{
    public interface IWebClient
    {
        string GetResponse(Uri uri);
    }
}
