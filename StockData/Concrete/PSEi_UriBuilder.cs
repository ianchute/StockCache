﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockData.Concrete
{
    public class PSEi_UriBuilder : IUriBuilder
    {
        public Uri Build(string endpoint, string parameter)
        {
            string uriString = endpoint + parameter;

            Uri result;
            if(Uri.TryCreate(uriString, UriKind.Absolute, out result))
                return result;
            return null;
        }
    }
}
