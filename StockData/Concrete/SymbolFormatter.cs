﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockData.Concrete
{
    public class SymbolFormatter : IFormatter<string>
    {
        public string Format(string _)
        {
            return _.ToUpper();
        }
    }
}