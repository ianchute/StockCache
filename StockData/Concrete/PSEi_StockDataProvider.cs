﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using StockData.Models;

namespace StockData.Concrete
{
    public class PSEi_StockDataProvider : IStockDataProvider
    {
        private IParameterBuilder parameterBuilder;
        private IUriBuilder uriBuilder;
        private IWebClient webClient;
        private IMapper<string, Stock> mapper;

        public PSEi_StockDataProvider(
            IParameterBuilder parameterBuilder,
            IUriBuilder uriBuilder,
            IWebClient webClient,
            IMapper<string, Stock> mapper)
        {
            this.parameterBuilder = parameterBuilder;
            this.uriBuilder = uriBuilder;
            this.webClient = webClient;
            this.mapper = mapper;
        }

        public Stock GetStockData(string symbol, DateTime date)
        {
            string endpoint = Settings.Endpoint;
            string parameter = parameterBuilder.Build(symbol, date);
            Uri uri = uriBuilder.Build(endpoint, parameter);

            string response = webClient.GetResponse(uri);

            Stock stockData = mapper.Map(response);

            return stockData;
        }
    }
}