﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using StockData.Models;

namespace StockData.Concrete
{
    public class PSEi_XML_StringResponseToStockMapper : IMapper<string, Stock>
    {
        public Stock Map(string _)
        {
            Stock result = new Stock();

            if(string.IsNullOrWhiteSpace(_))
                return result;

            try
            {
                XDocument x = XDocument.Parse(_);

                string xmlns = x.Elements().First().Attribute(XName.Get("xmlns")).Value;

                XElement rootElement = x.Element(XName.Get("stocks", xmlns));
                XElement stockElement = rootElement.Element(XName.Get("stock", xmlns));

                result.Symbol = stockElement.Attribute(XName.Get("symbol")).Value;
                result.Name = stockElement.Element(XName.Get("name", xmlns)).Value;
                result.Currency = stockElement.Element(XName.Get("price", xmlns)).Element(XName.Get("currency", xmlns)).Value;

                string asOfString = rootElement.Attribute(XName.Get("as_of")).Value;
                string amountString = stockElement.Element(XName.Get("price", xmlns)).Element(XName.Get("amount", xmlns)).Value;
                string volumeString = stockElement.Element(XName.Get("volume", xmlns)).Value;

                result.AsOf = DateTime.Parse(asOfString);
                result.SharePrice = double.Parse(amountString);
                result.Volume = int.Parse(volumeString);
                result.IsValid = true;
            }
            catch
            {
                Stock invalidStock = new Stock()
                {
                    IsValid = false
                };
                return invalidStock;
            }

            return result;
        }
    }
}
