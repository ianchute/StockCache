﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockData.Concrete
{
    public class PSEi_ParameterBuilder : IParameterBuilder
    {
        private IFormatter<string> symbolFormatter;
        private IFormatter<DateTime> dateFormatter;

        public PSEi_ParameterBuilder(IFormatter<string> symbolFormatter, IFormatter<DateTime> dateFormatter)
        {
            this.symbolFormatter = symbolFormatter;
            this.dateFormatter = dateFormatter;
        }

        public string Build(string symbol, DateTime date)
        {
            string symbolString = symbolFormatter.Format(symbol);
            string dateString = dateFormatter.Format(date);
            string responseFormatString = Settings.ResponseFormat;

            return string.Join(".", new[] { symbolString, dateString, responseFormatString });
        }
    }
}
