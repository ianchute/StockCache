﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockData.Concrete
{
    public class ShortDateFormatter : IFormatter<DateTime>
    {
        public string Format(DateTime _)
        {
            string yearString = _.Year.ToString().PadLeft(4, '0');
            string monthString = _.Month.ToString().PadLeft(2, '0');
            string dayString = _.Day.ToString().PadLeft(2, '0');
            string separator = "-";
            string result = string.Join(separator, yearString, monthString, dayString);

            return result;
        }
    }
}
