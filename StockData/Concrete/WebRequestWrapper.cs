﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace StockData.Concrete
{
    public class WebRequestWrapper : IWebClient
    {
        public string GetResponse(Uri uri)
        {
            try
            {
                WebRequest request = WebRequest.CreateHttp(uri);

                request.Method = Settings.ClientMethod;
                request.Proxy = Settings.Proxy;
                request.Timeout = Settings.ClientTimeout;

                WebResponse response = request.GetResponse();
                using(Stream responseStream = response.GetResponseStream())
                {
                    string result = null;

                    if(responseStream.CanRead)
                    {
                        using (StreamReader reader = new StreamReader(responseStream))
                        {
                            result = reader.ReadToEnd();
                        }
                    }

                    return result;
                }
            }
            catch
            {
                return null;
            }
        }
    }
}