﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockData
{
    public interface IUriBuilder
    {
        Uri Build(string endpoint, string parameter);
    }
}