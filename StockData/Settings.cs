﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace StockData
{
    public static class Settings
    {
        static NameValueCollection _ = ConfigurationManager.AppSettings;

        public static string Endpoint
        {
            get
            {
                return _["endpoint"].ToLowerInvariant();
            }
        }

        public static int ClientTimeout
        {
            get
            {
                int result = 5000;
                string timeout = _["clientTimeout"];
                int.TryParse(timeout, out result);
                return result;
            }
        }

        public static string ClientMethod
        {
            get
            {
                return _["clientMethod"].ToUpperInvariant();
            }
        }

        public static IWebProxy Proxy
        {
            get
            {
                string proxy = _["proxy"];
                if (proxy == "default")
                    return WebProxy.GetDefaultProxy();
                else if (proxy == "none" || proxy == "null")
                    return null;
                else if (proxy == "empty")
                    return GlobalProxySelection.GetEmptyWebProxy();
                else
                    return new WebProxy(proxy);
            }
        }

        public static string ResponseFormat
        {
            get
            {
                return _["responseFormat"].ToLowerInvariant();
            }
        }
    }
}
