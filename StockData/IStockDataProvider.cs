﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StockData.Models;

namespace StockData
{
    public interface IStockDataProvider
    {
        Stock GetStockData(string symbol, DateTime date);
    }
}
