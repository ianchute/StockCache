﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StockData.Concrete;
using StockData.Models;

namespace StockData.Tests
{
    [TestClass]
    public class PSEi_XML_StringResponseToStockMapperTests
    {
        PSEi_XML_StringResponseToStockMapper target;

        [TestInitialize]
        public void Initialize()
        {
            target = new PSEi_XML_StringResponseToStockMapper();
        }

        [TestCleanup]
        public void Cleanup()
        {
            target = null;
        }

        [TestMethod]
        public void PSEi_XML_StringResponseToStockMapperShouldConstruct()
        {
        }

        [TestMethod]
        public void PSEi_XML_StringResponseToStockMapperMapsCorrectly()
        {
            string sample = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" 
                + "<stocks xmlns=\"http://phisix-api.appspot.com/phisix-stocks\" as_of=\"2014-07-22T00:00:00+08:00\">" 
                + "<stock symbol=\"ALI\">" 
                + "<name>Ayala Land</name>"
                + "<price><currency>PHP</currency><amount>30.75</amount></price>" 
                + "<volume>4350500</volume></stock></stocks>";

            Stock result = target.Map(sample);

            Assert.AreEqual("PHP", result.Currency);
            Assert.AreEqual("Ayala Land", result.Name);
            Assert.AreEqual(30.75, result.SharePrice);
            Assert.AreEqual("ALI", result.Symbol);
            Assert.AreEqual(4350500, result.Volume);
            Assert.AreEqual("2014-07-22", result.AsOf.ToShortDateString());
            Assert.AreEqual(true, result.IsValid);
        }

        [TestMethod]
        public void PSEi_XML_StringResponseToStockMapperMapsInvalidWhenXmlIsInvalid()
        {
            string sample = "<?xml\"UTF-8\" standalone=\"yes\"?>"
                + "<stocks om/phisix-stocks\" as_of=\"2014-07-22T00:00:00+08:00\">"
                + "<stock symbol=\"ALI\">"
                + "<name>Ayala name>"
                + "<price><currency>y><amount>30.75</amount></price>"
                + "<volume>435k></stocks>";

            Stock result = target.Map(sample);

            AssertMapsInvalid(result);
        }

        [TestMethod]
        public void PSEi_XML_StringResponseToStockMapperMapsInvalidWhenXmlIsNull()
        {
            string sample = null;

            Stock result = target.Map(sample);

            AssertMapsInvalid(result);
        }

        private void AssertMapsInvalid(Stock result)
        {
            Assert.AreEqual(null, result.Currency);
            Assert.AreEqual(null, result.Name);
            Assert.AreEqual(0.0, result.SharePrice);
            Assert.AreEqual(null, result.Symbol);
            Assert.AreEqual(0, result.Volume);
            Assert.AreEqual(default(DateTime).ToShortDateString(), result.AsOf.ToShortDateString());
            Assert.AreEqual(false, result.IsValid);
        }

    }
}
