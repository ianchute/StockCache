﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StockData.Concrete;

namespace StockData.Tests
{
    [TestClass]
    public class SymbolFormatterTests
    {

        SymbolFormatter target;

        [TestInitialize]
        public void Initialize()
        {
            target = new SymbolFormatter();
        }

        [TestCleanup]
        public void Cleanup()
        {
            target = null;
        }

        [TestMethod]
        public void SymbolFormatterShouldConstruct()
        {
        }

        [TestMethod]
        public void FormatShouldReturnAllCaps()
        {
            string sample = "ali";

            string result = target.Format(sample);

            Assert.AreEqual("ALI", result);
        }
    }
}