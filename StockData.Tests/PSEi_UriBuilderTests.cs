﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StockData.Concrete;

namespace StockData.Tests
{
    [TestClass]
    public class PSEi_UriBuilderTests
    {
        PSEi_UriBuilder target;

        [TestInitialize]
        public void Initialize()
        {
            target = new PSEi_UriBuilder();
        }

        [TestCleanup]
        public void Cleanup()
        {
            target = null;
        }

        [TestMethod]
        public void PSEi_UriBuilderShouldConstruct()
        {
        }

        [TestMethod]
        public void BuildShouldReturnAppendedString()
        {
            string sampleEndpoint = "http://phisix-api.appspot.com/stocks/";
            string sampleParameter = "ALI.2014-07-22.json";

            Uri result = target.Build(sampleEndpoint, sampleParameter);

            Assert.AreEqual("http://phisix-api.appspot.com/stocks/ALI.2014-07-22.json", result.AbsoluteUri);
        }

        [TestMethod]
        public void BuildShouldReturnNullWhenInputsAreInvalid()
        {
            string sampleEndpoint = "abcd";
            string sampleParameter = "ALI.2014-07-22.json";

            Uri result = target.Build(sampleEndpoint, sampleParameter);

            Assert.IsNull(result);
        }
    }
}