﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StockData.Concrete;
using NSubstitute;
using NSubstitute.Core;
using System.Configuration;

namespace StockData.Tests
{
    [TestClass]
    public class PSEi_ParameterBuilderTests
    {
        PSEi_ParameterBuilder target;
        IFormatter<string> symbolFormatter;
        IFormatter<DateTime> dateFormatter;

        [TestInitialize]
        public void Initialize()
        {
            symbolFormatter = Substitute.For<IFormatter<string>>();
            symbolFormatter
                .Format(Arg.Any<string>())
                .Returns(_ => { return _.Arg<string>(); }); 

            dateFormatter = Substitute.For<IFormatter<DateTime>>();
            dateFormatter
                .Format(Arg.Any<DateTime>())
                .Returns<string>(_ => { return _.Arg<DateTime>().ToShortDateString(); });

            ConfigurationManager.AppSettings["responseFormat"] = "xml";

            target = new PSEi_ParameterBuilder(symbolFormatter, dateFormatter);
        }

        [TestCleanup]
        public void Cleanup()
        {
            target = null;
            ConfigurationManager.AppSettings["responseFormat"] = string.Empty;
        }

        [TestMethod]
        public void PSEi_ParameterBuilderShouldConstruct()
        {
        }

        [TestMethod]
        public void FormatShouldReturnDotSeparated()
        {
            string sampleSymbol = "ALI";
            DateTime sampleDate = new DateTime(1991, 10, 31);

            string result = target.Build(sampleSymbol, sampleDate);

            Assert.AreEqual("ALI.1991-10-31.xml", result);
        }
    }
}