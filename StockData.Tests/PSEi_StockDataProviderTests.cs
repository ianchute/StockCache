﻿using System;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using StockData.Concrete;
using StockData.Models;

namespace StockData.Tests
{
    [TestClass]
    public class PSEi_StockDataProviderTests
    {
        PSEi_StockDataProvider target;
        IParameterBuilder parameterBuilder;
        IUriBuilder uriBuilder;
        IWebClient webClient;
        IMapper<string, Stock> mapper;
        Stock sampleStock;


        [TestInitialize]
        public void Initialize()
        {
            parameterBuilder = Substitute.For<IParameterBuilder>();
            parameterBuilder.Build("ALI", new DateTime(2014, 07, 22))
                .Returns("param");

            ConfigurationManager.AppSettings["endpoint"] = "http://someendpoint.com/";

            uriBuilder = Substitute.For<IUriBuilder>();
            uriBuilder.Build(Settings.Endpoint, "param")
                .Returns(new Uri(Settings.Endpoint + "param"));

            webClient = Substitute.For<IWebClient>();
            webClient.GetResponse(Arg.Is<Uri>(_ => _.AbsoluteUri.Equals(Settings.Endpoint + "param")))
                .Returns("xml");

            sampleStock = new Stock()
            {
                AsOf = new DateTime(2014, 07, 22),
                Currency = "PHP",
                Name = "Ayala Land",
                SharePrice = 30.75,
                Symbol = "ALI",
                Volume = 4350500,
                IsValid = true
            };

            mapper = Substitute.For<IMapper<string, Stock>>();
            mapper.Map("xml")
                .Returns(sampleStock);

            target = new PSEi_StockDataProvider(parameterBuilder, uriBuilder, webClient, mapper);
        }

        [TestCleanup]
        public void Cleanup()
        {
            target = null;
        }

        [TestMethod]
        public void PSEi_StockDataProviderShouldConstruct()
        {
        }

        [TestMethod]
        public void PSEi_StockDataProviderGetsStockDataCorrectly()
        {
            string sampleSymbol = "ALI";
            DateTime sampleDate = new DateTime(2014, 07, 22);

            Stock result = target.GetStockData(sampleSymbol, sampleDate);

            Assert.AreEqual(sampleStock.Currency, result.Currency);
            Assert.AreEqual(sampleStock.Name, result.Name);
            Assert.AreEqual(sampleStock.SharePrice, result.SharePrice);
            Assert.AreEqual(sampleStock.Symbol, result.Symbol);
            Assert.AreEqual(sampleStock.Volume, result.Volume);
            Assert.AreEqual(sampleStock.AsOf.ToShortDateString(), result.AsOf.ToShortDateString());
            Assert.AreEqual(true, result.IsValid);
        }
    }
}