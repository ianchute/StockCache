﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StockData.Concrete;

namespace StockData.Tests
{
    [TestClass]
    public class ShortDateFormatterTests
    {
        ShortDateFormatter target;

        [TestInitialize]
        public void Initialize()
        {
            target = new ShortDateFormatter();
        }

        [TestCleanup]
        public void Cleanup()
        {
            target = null;
        }

        [TestMethod]
        public void ShortDateFormatterShouldConstruct()
        {
        }

        [TestMethod]
        public void FormatShouldReturnDashSeparated()
        {
            DateTime sample = new DateTime(1991, 01, 10);

            string result = target.Format(sample);

            Assert.AreEqual("1991-01-10", result);
        }
    }
}
